package com.horse.customlauncher;


import android.content.Context;
import android.content.pm.PackageManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.horse.customlauncher.models.AppLauncher;

import java.util.ArrayList;
import java.util.HashSet;


/**
 * Created by jass
 */
public class

SelectLaunchersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private ArrayList<AppLauncher> appLaunchers;
    private int lastPosition = -1;
    //private HashSet<Integer> selectedKeys = new HashSet<>();
    private ArrayList<Boolean> isSelected = new ArrayList<>();

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        LayoutInflater li = LayoutInflater.from(parent.getContext());
        mContext = parent.getContext();
        View itemView = li.inflate(R.layout.item_app_launcher, parent, false);
        return new ItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
        itemViewHolder.launcher_label.setText(appLaunchers.get(position).getName());
        //itemViewHolder.launcher_icon.setImageDrawable(appLaunchers.get(position).getDrawable());
        try {
            itemViewHolder.launcher_icon.setImageDrawable(mContext.getPackageManager().getApplicationIcon(appLaunchers.get(position).getPackageName()));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if (isSelected.get(position)) {
            itemViewHolder.launcher_check.setVisibility(View.VISIBLE);
        } else {
            itemViewHolder.launcher_check.setVisibility(View.GONE);
        }


        setAnimation(holder.itemView, position);
    }


    @Override
    public int getItemCount() {
        return appLaunchers == null ? 0 : appLaunchers.size();
    }

   public void resetData(ArrayList<AppLauncher> appLaunchers) {
        this.appLaunchers = appLaunchers;
        isSelected.clear();
        for (int i = 0; i < appLaunchers.size(); i++) {
            isSelected.add(false);
        }
       notifyDataSetChanged();
    }

    private void toggleItemSelection(Boolean select, int position) {
        if (select) {
            isSelected.set(position, true);
        } else {
            isSelected.set(position, false);
        }

        notifyItemChanged(position);
    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    /**
     * The type Item view holder.
     */
    public class ItemViewHolder extends RecyclerView.ViewHolder {

        private final RelativeLayout launcher_holder;
        private final ImageView launcher_icon, launcher_check;
        private final TextView launcher_label;

        /**
         * Instantiates a new Item view holder.
         *
         * @param itemView the item view
         */
        ItemViewHolder(final View itemView) {
            super(itemView);
            launcher_holder = itemView.findViewById(R.id.launcher_holder);
            launcher_icon = itemView.findViewById(R.id.launcher_icon);
            launcher_check = itemView.findViewById(R.id.launcher_check);
            launcher_label = itemView.findViewById(R.id.launcher_label);

            launcher_holder.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    boolean isSel = true;
                    if (isSelected.get(getAdapterPosition())) {
                        isSel = false;
                    }
                    toggleItemSelection(isSel, getAdapterPosition());
                    return true;
                }
            });
        }
    }

    public ArrayList<Boolean> getSelectedLaunchersArray() {
        return isSelected;
    }

}

