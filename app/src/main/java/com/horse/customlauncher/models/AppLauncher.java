package com.horse.customlauncher.models;

import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;

public class AppLauncher implements Parcelable {

    private int id;
    private String name;
    private String packageName;
   // private Drawable drawable;

    public AppLauncher(int id, String name, String packageName) {
        this.id = id;
        this.name = name;
        this.packageName = packageName;
        //this.drawable = drawable;
    }

    protected AppLauncher(Parcel in) {
        id = in.readInt();
        name = in.readString();
        packageName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(packageName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AppLauncher> CREATOR = new Creator<AppLauncher>() {
        @Override
        public AppLauncher createFromParcel(Parcel in) {
            return new AppLauncher(in);
        }

        @Override
        public AppLauncher[] newArray(int size) {
            return new AppLauncher[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

//    public Drawable getDrawable() {
//        return drawable;
//    }

    //public void setDrawable(Drawable drawable) {
        //this.drawable = drawable;
    //}
}
