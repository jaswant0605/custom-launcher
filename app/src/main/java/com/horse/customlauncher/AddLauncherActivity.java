package com.horse.customlauncher;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.LauncherActivityInfo;
import android.content.pm.LauncherApps;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.horse.customlauncher.database.CommonData;
import com.horse.customlauncher.models.AppLauncher;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.horse.customlauncher.constants.AppConstants.ALLOWED_APPS;

public class AddLauncherActivity extends AppCompatActivity implements View.OnClickListener {

    private ArrayList<AppLauncher> allowedAppLaunchers, nonDisplayedLaunchers;
    private RecyclerView rvNonIncludedLaunchers;
    private SelectLaunchersAdapter selectLaunchersAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_launcher);

        rvNonIncludedLaunchers = findViewById(R.id.rvNonIncludedLaunchers);
        selectLaunchersAdapter = new SelectLaunchersAdapter();
        rvNonIncludedLaunchers.setLayoutManager(new GridLayoutManager(this, 4));
        rvNonIncludedLaunchers.setAdapter(selectLaunchersAdapter);
        findViewById(R.id.btnAdd).setOnClickListener(this);
        initData();
        fetchNonDisplayedLaunchers();
    }

    private void fetchNonDisplayedLaunchers() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                nonDisplayedLaunchers = getNonDisplayedLaunchers(allowedAppLaunchers);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setDataToRecycler();
                    }
                });
            }
        }).start();
    }

    private void setDataToRecycler() {
        selectLaunchersAdapter.resetData(nonDisplayedLaunchers);
    }

    private ArrayList<AppLauncher> getNonDisplayedLaunchers(ArrayList<AppLauncher> displayedLaunchers) {

        ArrayList<String> alreadySelectedPackages = new ArrayList<>();
        for (int i = 0; i < displayedLaunchers.size(); i++) {
            alreadySelectedPackages.add(displayedLaunchers.get(i).getPackageName());
        }

        ArrayList<AppLauncher> allApps = new ArrayList<>();
        Intent intent = new Intent(Intent.ACTION_MAIN, null);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ResolveInfo> list = getPackageManager().queryIntentActivities(intent, PackageManager.PERMISSION_GRANTED);
        for (int i = 0; i < list.size(); i++) {
            ApplicationInfo componentInfo = list.get(i).activityInfo.applicationInfo;
            String label = componentInfo.loadLabel(getPackageManager()).toString();
            String packageName = componentInfo.packageName;
            try {
                LauncherApps launcher = (LauncherApps) getSystemService(Context.LAUNCHER_APPS_SERVICE);
                LauncherActivityInfo activityList = launcher.getActivityList(packageName, android.os.Process.myUserHandle()).get(0);
            } catch (Exception ignored) {}

            AppLauncher appLauncher = new AppLauncher(0, label, packageName);
            if (!alreadySelectedPackages.contains(packageName)) {
                allApps.add(appLauncher);
            }
        }

        Collections.sort(allApps, new Comparator<AppLauncher>() {
            @Override
            public int compare(AppLauncher o1, AppLauncher o2) {
                int res = String.CASE_INSENSITIVE_ORDER.compare(o1.getName(), o2.getName());
                if (res == 0) {
                    res = o1.getName().compareTo(o2.getName());
                }
                return res;
            }
        });



        return allApps;
    }

    private void initData() {
        allowedAppLaunchers = getIntent().getParcelableArrayListExtra(ALLOWED_APPS);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnAdd) {
            ArrayList<Boolean> newlySelected = selectLaunchersAdapter.getSelectedLaunchersArray();
            if (newlySelected.contains(true)) {
                allToDatabase(newlySelected);
            } else {
                Toast.makeText(this, "Please select some launcher to add", Toast.LENGTH_SHORT).show();
            }
        } else {
            Log.d("", "");
        }
    }

    private void allToDatabase(ArrayList<Boolean> newlySelected) {
        for (int i = 0; i < newlySelected.size(); i++) {
            if (newlySelected.get(i)) {
                allowedAppLaunchers.add(nonDisplayedLaunchers.get(i));
            }
        }
        CommonData.saveData(ALLOWED_APPS, allowedAppLaunchers);
        setResult(RESULT_OK);
        finish();
    }
}
