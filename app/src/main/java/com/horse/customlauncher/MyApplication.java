package com.horse.customlauncher;

import android.app.Application;

import io.paperdb.Paper;

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        //Initializing Paper DB (Local Database)
        Paper.init(getApplicationContext());
    }
}
