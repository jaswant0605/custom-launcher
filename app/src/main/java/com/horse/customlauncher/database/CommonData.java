package com.horse.customlauncher.database;

import io.paperdb.Paper;

public class CommonData {

    public static void saveData(String key, Object data) {
        Paper.book().write(key, data);
    }

    public static Object readData(String key) {
        return Paper.book().read(key);
    }

    public static void removeAllData() {
        Paper.book().destroy();
    }

    public static void deleteSomeData(String key) {
        Paper.book().delete(key);
    }
}
