package com.horse.customlauncher.constants;

public interface AppConstants {
    String ALLOWED_APPS = "allowedApps";
    String UNSELECTED_APPS = "unselectedApps";
    String NEWLY_SELECTED = "newlySelected";
}
