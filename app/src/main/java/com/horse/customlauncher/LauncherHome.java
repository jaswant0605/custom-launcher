package com.horse.customlauncher;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.horse.customlauncher.database.CommonData;
import com.horse.customlauncher.models.AppLauncher;

import java.util.ArrayList;

import static com.horse.customlauncher.constants.AppConstants.ALLOWED_APPS;
import static com.horse.customlauncher.constants.AppConstants.NEWLY_SELECTED;
import static com.horse.customlauncher.constants.AppConstants.UNSELECTED_APPS;

public class LauncherHome extends AppCompatActivity implements View.OnClickListener {

    private ArrayList<AppLauncher> allowedAppLaunchers;
    private LaunchersAdapter launchersAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher_home);

        initViews();
        initAppLaunchers();

    }

    private void initViews() {
        findViewById(R.id.fbAddLaunchers).setOnClickListener(this);

        launchersAdapter = new LaunchersAdapter() {
            @Override
            void onRemoveClicked(int Position) {
                allowedAppLaunchers.remove(Position);
                CommonData.saveData(ALLOWED_APPS, allowedAppLaunchers);
                launchersAdapter.resetData(allowedAppLaunchers);
            }
        };

        RecyclerView rvAllowedApps = findViewById(R.id.rvAllowedApps);
        rvAllowedApps.setLayoutManager(new GridLayoutManager(this, 4));
        rvAllowedApps.setAdapter(launchersAdapter);
    }

    private void initAppLaunchers() {
        try {
            allowedAppLaunchers = (ArrayList<AppLauncher>) CommonData.readData(ALLOWED_APPS);
            if (allowedAppLaunchers != null && allowedAppLaunchers.size() > 0) {
                launchersAdapter.resetData(allowedAppLaunchers);
            } else {
                allowedAppLaunchers = new ArrayList<>();
                Toast.makeText(this, "No Launchers Added!, please add some to start using", Toast.LENGTH_SHORT).show();
            }
        } catch (ClassCastException ignored) {
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fbAddLaunchers:
                Intent allLauncherIntent = new Intent(LauncherHome.this, AddLauncherActivity.class);
                allLauncherIntent.putParcelableArrayListExtra(ALLOWED_APPS, allowedAppLaunchers);
                startActivityForResult(allLauncherIntent, 10);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 10) {
            allowedAppLaunchers = (ArrayList<AppLauncher>) CommonData.readData(ALLOWED_APPS);
            launchersAdapter.resetData(allowedAppLaunchers);
        }
    }
}
