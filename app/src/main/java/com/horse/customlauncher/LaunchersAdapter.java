package com.horse.customlauncher;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.horse.customlauncher.models.AppLauncher;

import java.util.ArrayList;


/**
 * Created by jass
 */
public abstract class LaunchersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private ArrayList<AppLauncher> appLaunchers;
    private int lastPosition = -1;
   // private ArrayList<Boolean> isSelected = new ArrayList<>();

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        LayoutInflater li = LayoutInflater.from(parent.getContext());
        mContext = parent.getContext();
        View itemView = li.inflate(R.layout.item_app_launcher, parent, false);
        return new ItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
        itemViewHolder.launcher_label.setText(appLaunchers.get(position).getName());
        try {
            itemViewHolder.launcher_icon.setImageDrawable(mContext.getPackageManager().getApplicationIcon(appLaunchers.get(position).getPackageName()));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        setAnimation(holder.itemView, position);
    }


    @Override
    public int getItemCount() {
        return appLaunchers == null ? 0 : appLaunchers.size();
    }

   public void resetData(ArrayList<AppLauncher> appLaunchers) {
        this.appLaunchers = appLaunchers;

       notifyDataSetChanged();
    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    /**
     * The type Item view holder.
     */
    public class ItemViewHolder extends RecyclerView.ViewHolder {

        private final RelativeLayout launcher_holder;
        private final ImageView launcher_icon, launcher_check;
        private final TextView launcher_label;

        /**
         * Instantiates a new Item view holder.
         *
         * @param itemView the item view
         */
        ItemViewHolder(final View itemView) {
            super(itemView);
            launcher_holder = itemView.findViewById(R.id.launcher_holder);
            launcher_icon = itemView.findViewById(R.id.launcher_icon);
            launcher_check = itemView.findViewById(R.id.launcher_check);
            launcher_label = itemView.findViewById(R.id.launcher_label);


            launcher_holder.setOnClickListener(v -> {
                Intent openApplicationIntent = mContext.getPackageManager().getLaunchIntentForPackage(appLaunchers.get(getAdapterPosition()).getPackageName());
                mContext.startActivity(openApplicationIntent);
            });

            launcher_holder.setOnLongClickListener(view -> {
                final CharSequence[] items = {"Remove"};

                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

                builder.setTitle("Select The Action");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (item == 0) {
                            onRemoveClicked(getAdapterPosition());
                        }
                    }
                });
                builder.show();
                return true;
            });
        }
    }

    abstract void onRemoveClicked(int Position);
}

